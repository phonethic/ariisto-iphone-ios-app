//
//  AriistoAmenitiesImageViewController.h
//  Ariisto
//
//  Created by Rishi on 07/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoAmenitiesImageViewController : UIViewController {
    NSString *imageNameString;
}

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (id)initWithImageName:(NSString *)name;

@end
