//
//  AriistoFloorplanViewController.m
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoFloorplanViewController.h"

@interface AriistoFloorplanViewController ()

@end

@implementation AriistoFloorplanViewController
@synthesize floorplanScrollView;
@synthesize backBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"floorplan_img.png"]];
    imgView.frame = CGRectMake(0, 0, 320, 380);
    
	floorplanScrollView.maximumZoomScale = 3.0;
    floorplanScrollView.minimumZoomScale = 1.0;
	floorplanScrollView.clipsToBounds = YES;
	floorplanScrollView.delegate = self;
	floorplanScrollView.scrollEnabled = YES;
    [floorplanScrollView setZoomScale:1.0 animated:YES];
    [floorplanScrollView addSubview:imgView];
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imgView;
}

- (void)viewDidUnload
{
    [self setFloorplanScrollView:nil];
    [self setBackBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}
@end
