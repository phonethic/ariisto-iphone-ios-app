//
//  AriistoAmenitiesViewController.h
//  Ariisto
//
//  Created by Rishi on 06/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoAmenitiesViewController : UIViewController <UIScrollViewDelegate> {
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    int AmenityType;
}
@property (nonatomic, strong) NSMutableDictionary *amenitiesdata;
@property (nonatomic, retain) NSMutableArray *viewInternalControllers;
@property (nonatomic, retain) NSMutableArray *viewExternalControllers;
@property (nonatomic, assign) NSInteger AmenityType;

@property (strong, nonatomic) IBOutlet UIScrollView *internalScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *internalPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *externalScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *externalPgeControl;
@property (strong, nonatomic) IBOutlet UIButton *externalBtn;

- (void)loadScrollViewWithPage:(int)page type:(int)amtype;
- (void)unloadScrollViewWithPage:(int)page type:(int)amtype;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)toggleBtnPressed:(id)sender;

@end
