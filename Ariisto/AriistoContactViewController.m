//
//  AriistoContactViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoContactViewController.h"
#import "AriistoSiteViewController.h"

@interface AriistoContactViewController ()

@end

@implementation AriistoContactViewController
@synthesize helplineNumberBtn;
@synthesize emailBtn;
@synthesize websiteBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidUnload
{
    [self setHelplineNumberBtn:nil];
    [self setEmailBtn:nil];
    [self setWebsiteBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)presentEMailComposerView:(NSString*)To txt:(NSString*)textBody sub:(NSString*)subject
{
	
	@try
	{
		MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
		mfViewController.mailComposeDelegate = self;
		
		NSArray *toRecipients = [NSArray arrayWithObject:To];
		NSString *tempSubject= subject;
		
		NSString *tempBody = textBody;
		
		[mfViewController setSubject:tempSubject];
		[mfViewController setToRecipients:toRecipients];
		[mfViewController setMessageBody:tempBody isHTML:NO];
		
		[self presentModalViewController:mfViewController animated:YES];
		
	}
	@catch (NSException *exception)
	{
		NSLog(@"No email account set");
	}
	
}

- (void) mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error {
	
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)helplineBtnPressed:(id)sender {
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Ariisto"
                                                           message: @"Do you want to call our Customer Care Center number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:1];
        [ alert show ];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Ariisto"
                                                           message: @"Calling functionaltiy is not available on this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [alert setTag:2];
        [ alert show ];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 1)
    {
        if (buttonIndex == 1) {
            UIApplication *myApp = [UIApplication sharedApplication];
            [myApp openURL:[NSURL URLWithString:@"tel://912261300300"]];
            return;
        }
    } 
    
    
    
}

- (IBAction)emailBtnPressed:(id)sender {
    [self presentEMailComposerView:@"info@ariisto.com" txt:@"" sub:@""];
}

- (IBAction)websiteBtnPressed:(id)sender {
    AriistoSiteViewController *appointmentviewController = [[AriistoSiteViewController alloc] initWithNibName:@"AriistoSiteViewController" bundle:nil] ;
    appointmentviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:appointmentviewController animated:YES];
}
@end
