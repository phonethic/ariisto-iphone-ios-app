//
//  AriistoPDFViewController.h
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"

@interface AriistoPDFViewController : UIViewController <UIWebViewDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,ReaderViewControllerDelegate>{
    NSURLConnection *connection;
    NSMutableData *pdfData;
    float contentSize;
}
@property (strong, nonatomic) IBOutlet UIWebView *pdfWebView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIImageView *pdfView;

- (IBAction)backBtnPressed:(id)sender;
@end
