//
//  AriistoMyProjectViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoMyProjectViewController.h"
#import "AriistoDetailViewController.h"

@interface AriistoMyProjectViewController ()

@end

@implementation AriistoMyProjectViewController
@synthesize audioPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"i_button_sound" ofType:@"mp3"]] error:&error];
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
    }

}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)showGridView:(id)sender {
    [audioPlayer play];
    AriistoDetailViewController *detailviewController = [[AriistoDetailViewController alloc] initWithNibName:@"AriistoDetailViewController" bundle:nil] ;
    detailviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailviewController animated:YES];
}

@end
