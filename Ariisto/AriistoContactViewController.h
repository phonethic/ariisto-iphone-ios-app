//
//  AriistoContactViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoContactViewController : UIViewController  <MFMailComposeViewControllerDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UIButton *helplineNumberBtn;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
@property (strong, nonatomic) IBOutlet UIButton *websiteBtn;

- (IBAction)helplineBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;
- (IBAction)websiteBtnPressed:(id)sender;
@end
