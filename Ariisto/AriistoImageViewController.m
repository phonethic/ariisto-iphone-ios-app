//
//  AriistoImageViewController.m
//  Ariisto
//
//  Created by Rishi on 06/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoImageViewController.h"

@interface AriistoImageViewController ()

@end

@implementation AriistoImageViewController
@synthesize imgView;
@synthesize imagename;
@synthesize imagetype;

- (id)initWithImageOfType:(NSString *)imgName type:(NSString *)imgType
{
    if (self = [super initWithNibName:@"AriistoImageViewController" bundle:nil])
    {
        self.imagename = imgName;
        self.imagetype = imgType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:self.imagename ofType:self.imagetype]];
    [imgView setContentMode:UIViewContentModeTop];
    [self.view addSubview:imgView];
}

- (void)viewDidUnload
{
    [self setImgView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
