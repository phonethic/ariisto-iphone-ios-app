//
//  AriistoViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 26/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoViewController.h"

@interface AriistoViewController ()

@end

@implementation AriistoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
