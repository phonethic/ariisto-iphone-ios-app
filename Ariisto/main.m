//
//  main.m
//  Ariisto
//
//  Created by Sagar Mody on 26/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AriistoAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AriistoAppDelegate class]));
    }
}
