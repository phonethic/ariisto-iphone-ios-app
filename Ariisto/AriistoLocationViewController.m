//
//  AriistoLocationViewController.m
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoLocationViewController.h"

@interface AriistoLocationViewController ()

@end

@implementation AriistoLocationViewController
@synthesize locationScrollView;
@synthesize backBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locationmap.png"]];
    imgView.frame = CGRectMake(0, 0, 320, 380);
    
	locationScrollView.maximumZoomScale = 3.0;
    locationScrollView.minimumZoomScale = 1.0;
	locationScrollView.clipsToBounds = YES;
	locationScrollView.delegate = self;
	locationScrollView.scrollEnabled = YES;
    [locationScrollView setZoomScale:1.0 animated:YES];
    [locationScrollView addSubview:imgView];

}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imgView;
}


- (void)viewDidUnload
{
    [self setLocationScrollView:nil];
    [self setBackBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}
@end
