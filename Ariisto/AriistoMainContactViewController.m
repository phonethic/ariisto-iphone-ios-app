//
//  AriistoMainContactViewController.m
//  Ariisto
//
//  Created by Rishi on 12/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoMainContactViewController.h"
#import "AriistoSiteViewController.h"

@interface AriistoMainContactViewController ()

@end

@implementation AriistoMainContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)presentEMailComposerView:(NSString*)To txt:(NSString*)textBody sub:(NSString*)subject
{
	
	@try
	{
		MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
		mfViewController.mailComposeDelegate = self;
		
		NSArray *toRecipients = [NSArray arrayWithObject:To];
		NSString *tempSubject= subject;
		
		NSString *tempBody = textBody;
		
		[mfViewController setSubject:tempSubject];
		[mfViewController setToRecipients:toRecipients];
		[mfViewController setMessageBody:tempBody isHTML:NO];
		
		[self presentModalViewController:mfViewController animated:YES];
		
	}
	@catch (NSException *exception)
	{
		NSLog(@"No email account set");
	}
	
}

- (void) mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error {
	
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
     [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)helplinePressed:(id)sender {
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Ariisto"
                                                           message: @"Do you want to call our Customer Care Center number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:1];
        [ alert show ];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Ariisto"
                                                           message: @"Calling functionaltiy is not available on this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [alert setTag:2];
        [ alert show ];
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 1)
    {
        if (buttonIndex == 1) {
            UIApplication *myApp = [UIApplication sharedApplication];
            [myApp openURL:[NSURL URLWithString:@"tel://912261300300"]];
            return;
        }
    }
    
    
    
}


- (IBAction)addressPressed:(id)sender {
}

- (IBAction)emailPressed:(id)sender {
    [self presentEMailComposerView:@"info@ariisto.com" txt:@"" sub:@""];
}

- (IBAction)webPressed:(id)sender {
    AriistoSiteViewController *appointmentviewController = [[AriistoSiteViewController alloc] initWithNibName:@"AriistoSiteViewController" bundle:nil] ;
    appointmentviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:appointmentviewController animated:YES];

}
@end
