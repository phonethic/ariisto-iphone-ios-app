//
//  AriistoImageViewController.h
//  Ariisto
//
//  Created by Rishi on 06/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoImageViewController : UIViewController {
    NSString *imagename;
    NSString *imagetype;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic, copy) NSString *imagename;
@property (nonatomic, copy) NSString *imagetype;

- (id)initWithImageOfType:(NSString *)imgName type:(NSString *)imgType;

@end
