//
//  AriistoLayoutplanViewController.h
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoLayoutplanViewController : UIViewController <UIScrollViewDelegate>{
    UIImageView * imgView;
}

@property (strong, nonatomic) IBOutlet UIScrollView *layoutScrollView;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

- (IBAction)backBtnPressed:(id)sender;
    
@end
