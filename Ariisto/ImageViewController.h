//
//  ImageViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController {
    int pageNumber;
    int scrollType;
    UIImageView *imgView;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (id)initWithPageNumber:(int)page;
- (UIView *)getView;
-(void)setScrollType:(int)ltype;
@end
