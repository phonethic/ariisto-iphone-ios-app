//
//  AriistoProjectsData.h
//  Ariisto
//
//  Created by Rishi on 05/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AriistoProjectsData : NSObject {
    
}
@property (nonatomic, copy) NSString *projectName;
@property (nonatomic, copy) NSString *storeLink;
@end
