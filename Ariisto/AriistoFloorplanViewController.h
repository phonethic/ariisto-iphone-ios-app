//
//  AriistoFloorplanViewController.h
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoFloorplanViewController : UIViewController<UIScrollViewDelegate> {
    UIImageView * imgView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *floorplanScrollView;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

- (IBAction)backBtnPressed:(id)sender;
@end
