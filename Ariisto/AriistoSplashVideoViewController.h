//
//  AriistoSplashVideoViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AriistoSplashVideoViewController : UIViewController {
    MPMoviePlayerController *moviePlayer;
}

@end
