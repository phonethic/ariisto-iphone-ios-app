//
//  AriistoAppointmentViewController.m
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoAppointmentViewController.h"
#include <QuartzCore/QuartzCore.h>

@interface AriistoAppointmentViewController ()

@end

@implementation AriistoAppointmentViewController
@synthesize nameTxtField;
@synthesize emailTxtField;
@synthesize phoneTxtField;
@synthesize appointmentScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) presentMFMailComposerView {
	
	@try
	{
        MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
        mfViewController.mailComposeDelegate = self;
        
        NSArray *toRecipients = [NSArray arrayWithObject:@"sales@ariisto.com"];
        NSString *tempSubject= [NSString stringWithFormat:@""];
        
        NSString *tempBody=[NSString stringWithFormat:@"Name:\n%@ \n\nEmail:\n%@ \n\n Phone:\n%@",self.nameTxtField.text,self.emailTxtField.text,self.phoneTxtField.text];
        
        [mfViewController setSubject:tempSubject];
        [mfViewController setToRecipients:toRecipients];
        [mfViewController setMessageBody:tempBody isHTML:NO];
        
        [self presentModalViewController:mfViewController animated:YES];
        
    }
	@catch (NSException *exception) 
	{
		
	}
}

- (void) presentOnDeviceMailApp {
		
	NSString *body= [NSString stringWithFormat:@"Name:\n%@ \n\nEmail:\n%@ \n\n Phone:\n%@",self.nameTxtField.text,self.emailTxtField.text,self.phoneTxtField.text];
	NSLog(@"body::%@", body);
	
	NSString *email = [NSString stringWithFormat:@"%@",body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

- (void) mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error {
	
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == nameTxtField) {
        //[emailTxtField becomeFirstResponder];
	} else if (textField == emailTxtField) {
        [self scrollTotop:30];
	} else if (textField == phoneTxtField) {
        [self scrollTotop:110];
	}
}

-(void)scrollTotop : (int) value
{
    CGPoint topOffset = CGPointMake(0, value);
    [appointmentScrollView setContentOffset:topOffset animated:YES];
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [appointmentScrollView setContentOffset:bottomOffset animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == nameTxtField) {
        [emailTxtField becomeFirstResponder];
	} else if (textField == emailTxtField) {
        [phoneTxtField becomeFirstResponder];
	} else if (textField == phoneTxtField) {
        [phoneTxtField resignFirstResponder];
        [self scrollTobottom];
	}     
   	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appointmentScrollView.scrollEnabled = YES;
    appointmentScrollView.clipsToBounds = YES;
    appointmentScrollView.showsHorizontalScrollIndicator = NO;
    appointmentScrollView.showsVerticalScrollIndicator = NO;
    appointmentScrollView.scrollsToTop = YES;
    appointmentScrollView.delegate = self;
    appointmentScrollView.bounces = NO;
    [appointmentScrollView setContentSize:CGSizeMake(self.nameTxtField.frame.size.width, 340)];
    [self roundViewWithBorder:self.nameTxtField];
    [self roundViewWithBorder:self.phoneTxtField];
    [self roundViewWithBorder:self.emailTxtField];
}

-(void)roundViewWithBorder:(UITextField *)view
{
    view.layer.cornerRadius = 10;
    view.clipsToBounds = YES;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
}

- (void)viewDidUnload
{
    [self setNameTxtField:nil];
    [self setEmailTxtField:nil];
    [self setPhoneTxtField:nil];
    [self setAppointmentScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)sebdBtnPressed:(id)sender {
    [self presentMFMailComposerView];
    
    nameTxtField.text = @"";
    emailTxtField.text = @"";
    phoneTxtField.text = @"";

}
@end
