//
//  AriistoSiteViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 02/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoSiteViewController.h"

#define ARIISTO @"www.ariisto.com"

@interface AriistoSiteViewController ()

@end

@implementation AriistoSiteViewController
@synthesize ariistoWebView;
@synthesize loadingIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:@"http://ariisto.com/"];
	
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	//	[self setTitle:@"Account history"];
    ariistoWebView.delegate = self;
    //Load the request in the UIWebView.
    [ariistoWebView loadRequest:requestObj];
    ariistoWebView.dataDetectorTypes = UIDataDetectorTypeNone;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[loadingIndicator stopAnimating];    
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingIndicator startAnimating];
    [loadingIndicator setHidesWhenStopped:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@", [error localizedFailureReason]);
}


- (void)viewDidUnload
{
    [self setAriistoWebView:nil];
    [self setLoadingIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}
@end
