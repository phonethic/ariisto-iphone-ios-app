//
//  AriistoMapViewController.h
//  Ariisto
//
//  Created by Rishi on 03/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AriistoMapViewController : UIViewController <MKMapViewDelegate> {
     MKMapView *mapView;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)backBtn:(id)sender;

@end
