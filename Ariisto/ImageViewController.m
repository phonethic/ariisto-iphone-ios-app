//
//  ImageViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ImageViewController.h"


@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize imgView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setScrollType:(int)ltype
{
    scrollType = ltype;
}

- (id)initWithPageNumber:(int)page
{
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        NSLog(@"page init = %d",page + 1);
    }
    return self;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    if(scrollType == 7) {
        UITouch *touch = [touches anyObject];
        CGPoint pos = [touch locationInView: [UIApplication sharedApplication].keyWindow];
        
        if((pos.x > 42 &&  pos.x < 280) && (pos.y > 142 &&  pos.y < 286))
        {
            //NSLog(@"Position of touch: %.3f, %.3f", pos.x, pos.y);
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.view.backgroundColor = [UIColor yellowColor];
    
    CGRect frame = [[UIScreen mainScreen] bounds];
    NSLog(@"page width = %f",frame.size.width);
    NSLog(@"page height = %f",frame.size.height);
    //UIImageView *imgView = [[UIImageView alloc] initWithFrame:frame];
   // imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    if(scrollType == 0) {
        switch (pageNumber) {
            case 0:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sevenone_external_aminities_1x" ofType:@"jpg" ]];
                break;
                
            case 1:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sevenone_internal_aminities_1x" ofType:@"jpg" ]];
                break;
            
            default:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"glory" ofType:@"jpg" ]];
                break;
        }
        [imgView setContentMode:UIViewContentModeTop];
        [self.view addSubview:imgView];
    } else if(scrollType == 1) {
        switch (pageNumber) {
            case 0:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sapphire" ofType:@"jpg" ]];
                break;
                
            case 1:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cloud" ofType:@"jpg" ]];
                break;
                
                
            case 2:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"county" ofType:@"jpg" ]];
                break;
                
            case 3:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cozy" ofType:@"jpg" ]];
                break;
                
            case 4:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"glory" ofType:@"jpg" ]];
                break;
                
            case 5:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"heaven" ofType:@"jpg" ]];
                break;
                
            case 6:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"horizon" ofType:@"jpg" ]];
                break;
                
            case 7:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ria" ofType:@"jpg" ]];
                break;
                
            case 8:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"chambers" ofType:@"jpg" ]];
                break;
                
            case 9:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"centrum" ofType:@"jpg" ]];
                break;
                
            case 10:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"solitare" ofType:@"jpg" ]];
                break;
                
                
            default:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"glory" ofType:@"jpg" ]];
                break;
        }
        [imgView setContentMode:UIViewContentModeTop];
        [self.view addSubview:imgView];
    } else if(scrollType == 2) {
        switch (pageNumber) {
            case 0:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_1" ofType:@"jpg" ]];
                break;
                
            case 1:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_2" ofType:@"jpg" ]];
                break;
                
            case 2:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_3" ofType:@"jpg" ]];
                break;
                
            case 3:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_4" ofType:@"jpg" ]];
                break;
                
            default:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_1" ofType:@"jpg" ]];
                break;
        }
        [imgView setContentMode:UIViewContentModeTop];
        [self.view addSubview:imgView];
    } else if(scrollType == 3) {
        switch (pageNumber) {
            case 0:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sevenone_internal_aminities_1x" ofType:@"jpg" ]];
                break;
                
            case 1:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sevenone_external_aminities_1x" ofType:@"jpg" ]];
                break;
                
            case 2:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"elevation" ofType:@"png" ]];
                break;
                
            default:
                imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home_1" ofType:@"jpg" ]];
                break;
        }
        [imgView setContentMode:UIViewContentModeTop];
        [self.view addSubview:imgView];
    }


}

- (UIView *)getView {
    return imgView; // 
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
//        if(pageNumber == 0) {
//            [imgView setImage:[UIImage imageNamed:@"carservicestips.jpg"]];
//        }
//        
//    }
//    else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
//        if(pageNumber == 0) {
//            [imgView setImage:[UIImage imageNamed:@"NISSAN-SUNNY.jpg"]];
//        }
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
//    UIInterfaceOrientation orientation = [[UIDevice currentDevice] orientation];
//
//    if(orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
//        if(pageNumber == 0) {
//            [imgView setImage:[UIImage imageNamed:@"tips1.jpg"]];
//        }
//        
//    }
//    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
//        if(pageNumber == 0) {
//            [imgView setImage:[UIImage imageNamed:@"NISSAN-SUNNY.jpg"]];
//        }
//    }

}

- (void)viewDidUnload
{
    imgView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
