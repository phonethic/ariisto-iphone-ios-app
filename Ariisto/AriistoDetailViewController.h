//
//  AriistoDetailViewController.h
//  Ariisto
//
//  Created by Rishi on 29/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#define _GRID_VIEW_ "3x2"

@interface AriistoDetailViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate> {
    BOOL pageControlUsed;
    BOOL BUTTONFLAG;
    AVAudioPlayer *audioPlayer;
}
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIImageView *headerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pagecontrol;
@property (strong, nonatomic) IBOutlet UIButton *arrowBtn;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)arrowBtnPressed:(id)sender;
- (IBAction)changePage:(id)sender;

@end
