//
//  AriistoElevationViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 03/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoElevationViewController.h"

@interface AriistoElevationViewController ()

@end

@implementation AriistoElevationViewController
@synthesize toggleBtn;
@synthesize elevationScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"elevation_img.png"]];
    
	elevationScrollView.maximumZoomScale = 3.0;
    elevationScrollView.minimumZoomScale = 1.0;
	elevationScrollView.clipsToBounds = YES;
	elevationScrollView.delegate = self;
	elevationScrollView.scrollEnabled = YES;
    [elevationScrollView setZoomScale:1.0 animated:YES];
    [elevationScrollView addSubview:imgView];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imgView;
}


- (void)viewDidUnload
{
    [self setToggleBtn:nil];
    [self setElevationScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
      [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)toggleBtnPressed:(id)sender {
    UIButton *Btn = (UIButton *)sender;
    if([Btn.titleLabel.text isEqualToString:@"Day"])
    {
        [Btn setTitle: @"Night" forState: UIControlStateNormal];
        imgView.image = [UIImage imageNamed:@"elevation_building_img.png"];
        [Btn setImage:[UIImage imageNamed:@"day.png"] forState:UIControlStateNormal];
    } else {
        [Btn setTitle: @"Day" forState: UIControlStateNormal];
        imgView.image = [UIImage imageNamed:@"elevation_img.png"];
        [Btn setImage:[UIImage imageNamed:@"night.png"] forState:UIControlStateNormal];
    }
}


@end
