//
//  AriistoElevationViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 03/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoElevationViewController : UIViewController <UIScrollViewDelegate>{
    UIImageView * imgView;
}
@property (strong, nonatomic) IBOutlet UIButton *toggleBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *elevationScrollView;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)toggleBtnPressed:(id)sender;
@end
