//
//  AriistoAppDelegate.h
//  Ariisto
//
//  Created by Sagar Mody on 26/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

#define NO_STARTING_ANIMATION

#define myAppDelegate (AriistoAppDelegate*)[[UIApplication sharedApplication] delegate] 

@class AriistoSplashVideoViewController;
@class PanoormaAnimation;

@interface AriistoAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate> {
    Reachability* internetReach;
    Boolean networkavailable;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) AriistoSplashVideoViewController *animationviewController;
@property (strong, nonatomic) PanoormaAnimation *panoController;
-(void)animationStop;
-(void)panaromaStop;

@end
