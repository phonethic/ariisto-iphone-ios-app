//
//  AriistoGalleryViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoGalleryViewController.h"
#import "ImageViewController.h"
#import "AriistoDetailViewController.h"
#import "AriistoProjectsData.h"
#import "AriistoImageViewController.h"

#define PROJECTS_LINK @"http://192.168.254.13/ariisto_projects.xml"


@interface AriistoGalleryViewController ()
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;
@end

@implementation AriistoGalleryViewController
@synthesize detailsBtn;
@synthesize statusIndicator;
@synthesize scrollView, pageControl, viewControllers;
@synthesize page = _page;
@synthesize rotating = _rotating;
@synthesize audioPlayer;
@synthesize kNumberOfPages;
@synthesize ituneLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    UIViewController *viewController = [viewControllers objectAtIndex:pageControl.currentPage];
	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
	NSUInteger page = 0;
	for (viewController in viewControllers) {
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * page;
		frame.origin.y = 0;
		viewController.view.frame = frame;
		page++;
	}
	
	CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
	[self.scrollView scrollRectToVisible:frame animated:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	for (NSUInteger i =0; i < [viewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
    
	self.pageControl.currentPage = 0;
	_page = 0;
	[self.pageControl setNumberOfPages:kNumberOfPages];
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
    
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
}

- (void)changePage:(id)sender
{
    NSLog(@"Page changed");
    int page = pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
	return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    projectsImagesArray = [NSArray arrayWithObjects:@"sapphire",@"cloud",@"county" ,@"cozy",@"glory",@"heaven",@"horizon",@"ria",@"chambers",@"centrum",@"solitaire",nil];
    
    kNumberOfPages = [projectsImagesArray count];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"photo_slide_sound" ofType:@"mp3"]] error:&error];
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
    }
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    scrollView.clipsToBounds = YES;
	scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height + 280);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.directionalLockEnabled = YES;

    pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    //[self.view bringSubviewToFront:pageControl];
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    //
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    [self projectListAsynchronousCall];

    
}

-(void)projectListAsynchronousCall
{
    
    [statusIndicator startAnimating];
    [detailsBtn setHidden:TRUE];
    
	/****************Asynchronous Request**********************/
		
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:PROJECTS_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}

	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	//[self connectionDidFinishLoading:nil];
    [statusIndicator stopAnimating];
    [detailsBtn setHidden:FALSE];
    [self parseFromFile];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//NSLog(@"\n result:%@\n\n", result);
        [self writeToTextFile:result name:@"projects"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
	} else {
        [self parseFromFile];
    }
		
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [self getTextFromFile:@"projects"];
    NSLog(@"\n data:%@\n\n", data);
    if(![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
    } else {
        NSLog(@"\n null data\n");
        NSString* path = [[NSBundle mainBundle] pathForResource:@"ariisto_projects"
                                                         ofType:@"xml"];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:content];
    }
    
    NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
    [xmlParser setDelegate:self];
    [xmlParser parse];

}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"projects"])
	{
		projects = [[NSMutableArray alloc] init];
	} else if([elementName isEqualToString:@"project"]) {
        projectsObj = [[AriistoProjectsData alloc] init];
        projectsObj.projectName = [attributeDict objectForKey:@"name"];
    } else if([elementName isEqualToString:@"iphone_link"]) {
        linkFound = YES;
    }
}


-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (linkFound) {
        projectsObj.storeLink = string;
    }
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    linkFound = FALSE;
    if ([elementName isEqualToString:@"projects"]) {
        [statusIndicator stopAnimating];
        [detailsBtn setHidden:FALSE];
        [self reloadProjectStatus];
    } else if([elementName isEqualToString:@"project"]) {
        [projects addObject:projectsObj];
        projectsObj = nil;
    }
}


-(void) reloadProjectStatus {
    for(int i=0; i<[projects count];i++)
    {
        AriistoProjectsData *tempObj = [projects objectAtIndex:i];
        //NSLog(@"name = %@ link = %@ arrayname = %@", tempObj.projectName,tempObj.storeLink,[projectsImagesArray objectAtIndex:_page]);
        if([tempObj.projectName isEqualToString:[projectsImagesArray objectAtIndex:_page]])
        {
            self.ituneLink = tempObj.storeLink;
            if(tempObj.storeLink == nil) {
                [detailsBtn setImage:[UIImage imageNamed:@"coming-soon.png"] forState:UIControlStateNormal];
            }  else {
                [detailsBtn setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
            }
            break;
        }
    }
}

//Method writes a string to a text file
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.xml",lname];
    NSLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        //[self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
    if(error != nil)
        NSLog(@"write error %@", error);
    else {
        NSLog(@"file created %@", fileName);
    }
    
	
}

-(NSString *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.xml",lname];
    NSLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSLog(@"fullpath = %@" , fullPath);
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil)
            NSLog(@"write error %@", error);
        return data;
    } else {
        return @"";
    }
}

- (void)viewWillDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillDisappear:animated];
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
	[super viewDidDisappear:animated];
}

- (void)unloadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    UIViewController *controller = [viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller != [NSNull null]) {
        if (nil != controller.view.superview) {
            [controller.view removeFromSuperview];
            controller.view=nil;
        }
        
        [viewControllers replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (IBAction)detailsBtnPressed:(id)sender {
    NSLog(@"page =  %d",_page);
    if(self.ituneLink != nil)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ituneLink]];
    }
}

- (void)viewDidUnload
{

    [self setScrollView:nil];
    [self setPageControl:nil];
    [self setDetailsBtn:nil];
    [self setStatusIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        controller = (UIViewController *)[[AriistoImageViewController alloc] initWithImageOfType:[projectsImagesArray objectAtIndex:page] type:@"jpg"];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
    
	_page = self.pageControl.currentPage;
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
        [audioPlayer prepareToPlay];
        [audioPlayer play];
    }
    if (self.pageControl.currentPage != page) {
		UIViewController *oldViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
		UIViewController *newViewController = [viewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		self.pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    detailsBtn.hidden = TRUE;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    detailsBtn.hidden = FALSE;
    [self reloadProjectStatus];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
