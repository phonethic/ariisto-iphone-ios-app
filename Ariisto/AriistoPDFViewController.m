//
//  AriistoPDFViewController.m
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoPDFViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ReaderViewController.h"

@interface AriistoPDFViewController ()

@end

@implementation AriistoPDFViewController
@synthesize pdfWebView;
@synthesize progressBar;
@synthesize loadingView;
@synthesize backBtn;
@synthesize pdfView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [progressBar setProgress:0];
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString *filePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ariisto.pdf"]];

    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: filePath ] == NO)
    {
        NSLog (@"File not found");
        pdfView.hidden = FALSE;
        [self loadwebDocument];

    } else {
         NSLog (@"File exists");
        pdfView.hidden = TRUE;
        [self loadLocalDocument];
    }
    [self roundViewWithBorder:loadingView];
}

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
    contentSize = [httpResponse expectedContentLength];
    [pdfData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    if(pdfData != nil)
        [pdfData appendData:data];
    float progress = (float)[pdfData length] / (float)contentSize;
    [progressBar setProgress:progress];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    NSLog(@"Connection failed! Error - %@", [error localizedDescription]);

}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    NSLog(@"DONE. Received Bytes: %d", [pdfData length]);
    pdfView.hidden = TRUE; 
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString *filePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ariisto.pdf"]];
    
    BOOL pass = [pdfData writeToFile:filePath atomically:YES];
    if (pass) {
        NSLog(@"Saved to file: %@", filePath);
        [self loadLocalDocument];
    } else {
        NSLog(@"problem");
    }
    
}

-(void) roundViewWithBorder:(UIView *)view
{
    view.layer.cornerRadius = 10;
    view.clipsToBounds = YES;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
}

-(void) loadLocalDocument {
    
    [loadingView setHidden:YES];
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString *filePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ariisto.pdf"]];
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
    if (document != nil)
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentModalViewController:readerViewController animated:YES];
    }
    else
        NSLog(@"got nil");
    
//    NSURL *url = [NSURL fileURLWithPath:filePath];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    [pdfWebView setUserInteractionEnabled:YES];
//    [pdfWebView setDelegate:self];
//    [pdfWebView loadRequest:request];
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController {
    [self dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) loadwebDocument {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ariisto.com/pdf/Ariisto%20Sapphire%20Brochure.pdf"]];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        pdfData = [[NSMutableData alloc] init];
    }
}

- (void)viewDidUnload
{
    [self setPdfWebView:nil];
    [self setProgressBar:nil];
    [self setLoadingView:nil];
    [self setBackBtn:nil];
    [self setPdfView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [connection cancel];
    [self.navigationController popViewControllerAnimated: YES];
}
@end
