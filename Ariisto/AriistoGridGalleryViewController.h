//
//  AriistoGridGalleryViewController.h
//  Ariisto
//
//  Created by Rishi on 05/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AriistoGridGalleryViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    AVAudioPlayer *audioPlayer;
    NSUInteger kNumberOfPages;
    NSArray  *gridgalleryImagesArray;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
}


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;

- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
- (IBAction)backBtnPressed:(id)sender;

@end
