//
//  AriistoMainContactViewController.h
//  Ariisto
//
//  Created by Rishi on 12/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoMainContactViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)helplinePressed:(id)sender;
- (IBAction)addressPressed:(id)sender;
- (IBAction)emailPressed:(id)sender;
- (IBAction)webPressed:(id)sender;
@end
