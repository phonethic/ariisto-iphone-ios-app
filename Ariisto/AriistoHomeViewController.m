//
//  AriistoHomeViewController.m
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoHomeViewController.h"
#import "ImageViewController.h"
#import "AriistoImageViewController.h"

@interface AriistoHomeViewController ()
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;
@end

@implementation AriistoHomeViewController
@synthesize scrollView;
@synthesize pageControl;
@synthesize speakerBtn;
@synthesize volSlider;
@synthesize settingBtn;
@synthesize viewControllers;
@synthesize page = _page;
@synthesize rotating = _rotating;
@synthesize audioPlayer;
@synthesize textLabelsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    UIViewController *viewController = [viewControllers objectAtIndex:pageControl.currentPage];
	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
	NSUInteger page = 0;
	for (viewController in viewControllers) {
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * page;
		frame.origin.y = 0;
		viewController.view.frame = frame;
		page++;
	}
	
	CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
	[self.scrollView scrollRectToVisible:frame animated:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	for (NSUInteger i =0; i < [viewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
    
	self.pageControl.currentPage = 0;
	_page = 0;
	[self.pageControl setNumberOfPages:kNumberOfPages];
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
    if([speakerBtn.titleLabel.text isEqualToString:@"speaker_off"])
    {
        [audioPlayer pause];
    } else {
        [audioPlayer play];
    }
}

- (IBAction)changePage:(id)sender
{
    NSLog(@"Page changed");
    int page = pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (IBAction)speakerBtnPressed:(id)sender {
    if([speakerBtn.titleLabel.text isEqualToString:@"speaker_off"])
    {
        [audioPlayer play];
        [speakerBtn setTitle: @"speaker_on" forState: UIControlStateNormal];
        [speakerBtn setImage:[UIImage imageNamed:@"volume_on.png"] forState:UIControlStateNormal];
    } else {
        [audioPlayer pause];
        [speakerBtn setTitle: @"speaker_off" forState: UIControlStateNormal];
        [speakerBtn setImage:[UIImage imageNamed:@"volume_off.png"] forState:UIControlStateNormal];
    }
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
	return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    homeImagesArray = [NSArray arrayWithObjects:@"Home_1", @"Home_2", @"Home_3", @"Home_4", nil];
    
    kNumberOfPages = [homeImagesArray count];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Ariisto_web_music" ofType:@"mp3"]] error:&error];
    audioPlayer.numberOfLoops = -1;
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        audioPlayer.volume = 0.5;
        [volSlider setThumbImage:[UIImage imageNamed:@"knob.png"] forState:UIControlStateNormal];
        volSlider.value = audioPlayer.volume;
        volSlider.transform = CGAffineTransformRotate(volSlider.transform, 270.0/180*M_PI); 
        [speakerBtn setTitle: @"speaker_on" forState: UIControlStateNormal];
        [speakerBtn setImage:[UIImage imageNamed:@"volume_on.png"] forState:UIControlStateNormal];

    }
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    scrollView.clipsToBounds = YES;
	scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height + 280);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.directionalLockEnabled = YES;
    //scrollView.maximumZoomScale = 4.0;
	//scrollView.minimumZoomScale = 0.75;
    //scrollView.bouncesZoom = NO;
    //pageControl.frame = CGRectMake(0, 375, 320, 36);
    //pageControl.backgroundColor = [UIColor blackColor];
    //pageControl.numberOfPages = kNumberOfPages;
    //pageControl.currentPage = 0;
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    //[self.view bringSubviewToFront:pageControl];
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    //
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    [self createlabels:7];
    
    [self settingShowHide:0 animate:0];
    [speakerBtn setTitle: @"hide" forState: UIControlStateNormal];

}

-(void) settingShowHide:(int) value animate: (int) anim
{
    if(anim) {
        [UIView beginAnimations:@"Drop_Down" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
    }
    if(value) {
        volSlider.frame =  CGRectMake(volSlider.frame.origin.x - 50, volSlider.frame.origin.y, volSlider.frame.size.width, volSlider.frame.size.height);
        speakerBtn.frame = CGRectMake(speakerBtn.frame.origin.x - 50, speakerBtn.frame.origin.y, speakerBtn.frame.size.width, speakerBtn.frame.size.height);
    } else {
        volSlider.frame = CGRectMake(volSlider.frame.origin.x + 50, volSlider.frame.origin.y, volSlider.frame.size.width, volSlider.frame.size.height);
        speakerBtn.frame =  CGRectMake(speakerBtn.frame.origin.x + 50, speakerBtn.frame.origin.y, speakerBtn.frame.size.width, speakerBtn.frame.size.height);
    }
    if(anim)
        [UIView commitAnimations];
}
-(void)createlabels:(int) count {
    textLabelsArray = [[NSMutableArray alloc] initWithCapacity:8];
    NSMutableArray *captionArray = [NSMutableArray arrayWithObjects:@"FROM",@"SUPER",@"PREMIUM",@"TOWERS",@"TO",@"TOWNSHIPS",@"", nil];
    for(int i = 0; i < count; i++)
    {
        int Yaxis = 40 * i;
        UILabel *lblMyLable;
        if(Yaxis == 0)
            lblMyLable = [[UILabel alloc] initWithFrame:CGRectMake(100, 100 , 300, 50)];
        else
            lblMyLable = [[UILabel alloc] initWithFrame:CGRectMake(100, 100 + Yaxis , 300, 50)];
        
        lblMyLable.lineBreakMode = UILineBreakModeWordWrap;
        lblMyLable.numberOfLines = 0;//Dynamic
        lblMyLable.tag = 1;
        lblMyLable.textColor = [UIColor whiteColor];
        lblMyLable.backgroundColor = [UIColor clearColor];
        lblMyLable.text = [captionArray objectAtIndex:i];
        [lblMyLable setFont:[UIFont fontWithName:@"Courier" size:25.0]];
        [textLabelsArray addObject:lblMyLable];
    }
    [self addlabelsToView:count];
    
}

-(void) changeText:(int)page
{
    switch (page) {
        case 0: {
            NSMutableArray *captionArray = [NSMutableArray arrayWithObjects:@"FROM",@"SUPER",@"PREMIUM",@"TOWERS",@"TO",@"TOWNSHIPS",@"", nil];
            for(int i = 0; i < 7; i++)
            {
                UILabel *myLabel = [textLabelsArray objectAtIndex:i];
                myLabel.text = [captionArray objectAtIndex:i];
            }
        }
        break;
        case 1: {
            NSMutableArray *captionArray = [NSMutableArray arrayWithObjects:@"FROM",@"SUPER",@"LUXURY",@"RESIDENCES",@"TO",@"ECONOMICAL",@"HOMES", nil];
            for(int i = 0; i < 7; i++)
            {
                UILabel *myLabel = [textLabelsArray objectAtIndex:i];
                myLabel.text = [captionArray objectAtIndex:i];
            }
        }
        break;
        case 2: {
            NSMutableArray *captionArray = [NSMutableArray arrayWithObjects:@"FROM",@"BOUTIQUE",@"OFFICES",@"TO",@"LAVISH H.Os",@"",@"" ,nil];
            for(int i = 0; i < 7; i++)
            {
                UILabel *myLabel = [textLabelsArray objectAtIndex:i];
                myLabel.text = [captionArray objectAtIndex:i];
            }
        }
            break;
        case 3: {
            NSMutableArray *captionArray = [NSMutableArray arrayWithObjects:@"DEVELOPING",@"32 MILLION SQ.FT.",@"18 PROJECTS",@"1 CITY MUMBAI",@"",@"",@"", nil];
            for(int i = 0; i < 7; i++)
            {
                UILabel *myLabel = [textLabelsArray objectAtIndex:i];
                myLabel.text = [captionArray objectAtIndex:i];
            }
        }
        break;
            
        default:
            break;
    }
}
-(void)addlabelsToView:(int) count
{
    for(int i = 0; i < count; i++)
    {
        UILabel *myLabel = [textLabelsArray objectAtIndex:i];
        //NSLog(@" Y = %f",myLabel.frame.origin.y);
        [self.view addSubview:myLabel];
        [self.view bringSubviewToFront:myLabel];
        myLabel.alpha = 0;
        [self performSelector:@selector(backAnimate:) withObject:myLabel afterDelay:i*0.7];
        [self performSelector:@selector(forwardAnimate:) withObject:myLabel afterDelay:(i+6)*0.7];
        
    }
    [self scheduleTimer];
}

-(void)scheduleTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval: 8.5 target: self
                                           selector: @selector(timerCallback:) userInfo: nil repeats: YES];
}

-(void) timerCallback:(NSTimer*) t
{
    for(int i = 0; i < 7; i++)
    {
        UILabel *myLabel = [textLabelsArray objectAtIndex:i];
        [self performSelector:@selector(backAnimate:) withObject:myLabel afterDelay:i*0.7];
        [self performSelector:@selector(forwardAnimate:) withObject:myLabel afterDelay:(i+6)*0.7];
    }
}

-(void)fadeOut:(UIView*)viewToDissolve withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade Out" context:nil];
    // wait for time before begin
    [UIView setAnimationDelay:0];
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToDissolve.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade In" context:nil];
    // wait for time before begin
    [UIView setAnimationDelay:0];
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}

-(void)backCABasicAnimation:(UILabel *)lview
{
    [self fadeIn:lview withDuration:0.5];
    lview.alpha = 1.0;
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration = 0.8;
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    theAnimation.repeatCount = 1;
    theAnimation.autoreverses = NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:50];
    theAnimation.toValue=[NSNumber numberWithFloat:0];
    [theAnimation setRemovedOnCompletion:YES];
    [theAnimation setFillMode:kCAFillModeBackwards];
    [lview.layer addAnimation:theAnimation forKey:@"backanimateLayer"];
}

-(void)forwardCABasicAnimation:(UILabel *)lview
{
    [self fadeOut:lview withDuration:0.5];
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration = 0.8;
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    theAnimation.repeatCount = 1;
    theAnimation.autoreverses = NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:0];
    theAnimation.toValue=[NSNumber numberWithFloat:50];
    [theAnimation setRemovedOnCompletion:YES];
    [theAnimation setFillMode:kCAFillModeForwards];
    [lview.layer addAnimation:theAnimation forKey:@"fwdanimateLayer"];
}

-(void)backAnimate:(UILabel*)lview
{
    [self fadeIn:lview withDuration:0.5];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:lview cache:YES];
    [lview setFrame:CGRectMake(lview.frame.origin.x-50,lview.frame.origin.y,lview.frame.size.width,lview.frame.size.height)];
    [UIView commitAnimations];
}

-(void)forwardAnimate:(UILabel*)lview
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:lview cache:YES];
    [lview setFrame:CGRectMake(lview.frame.origin.x+50,lview.frame.origin.y,lview.frame.size.width,lview.frame.size.height)];
    [UIView commitAnimations];
    [self fadeOut:lview withDuration:0.5];
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

- (void)viewWillDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillDisappear:animated];
	}
    [audioPlayer pause];
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
	[super viewDidDisappear:animated];
}

- (void)unloadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    UIViewController *controller = [viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller != [NSNull null]) {
        if (nil != controller.view.superview) {
            [controller.view removeFromSuperview];
            controller.view=nil;
        }
        
        [viewControllers replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (IBAction)volChanged:(id)sender {
    UISlider *slider = (UISlider *) sender;
    audioPlayer.volume = slider.value;
}

- (IBAction)settingBtnPressed:(id)sender {
    if([speakerBtn.titleLabel.text isEqualToString:@"hide"])
    {
        [self settingShowHide:1 animate:1];
        [speakerBtn setTitle: @"show" forState: UIControlStateNormal];
    } else {
        [self settingShowHide:0 animate:1];
        [speakerBtn setTitle: @"hide" forState: UIControlStateNormal];
    }
    
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setPageControl:nil];
    [self setSpeakerBtn:nil];
    [self setVolSlider:nil];
    [self setSettingBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        controller = (UIViewController *)[[AriistoImageViewController alloc] initWithImageOfType:[homeImagesArray objectAtIndex:page] type:@"jpg"];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
    
	_page = self.pageControl.currentPage;
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
        //[audioPlayer prepareToPlay];
        //[audioPlayer play];
        [self changeText:page];
    }
    if (self.pageControl.currentPage != page) {
		UIViewController *oldViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
		UIViewController *newViewController = [viewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		self.pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    [timer invalidate];
    for(int i = 0; i < 7; i++)
    {
        UILabel *myLabel = [textLabelsArray objectAtIndex:i];
        //[myLabel.layer removeAllAnimations];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(backAnimate:) object:myLabel];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(forwardAnimate:) object:myLabel];
        myLabel.frame = CGRectMake(100, myLabel.frame.origin.y , myLabel.frame.size.width, myLabel.frame.size.height);
        myLabel.alpha = 0;
    }
    
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    for(int i = 0; i < 7; i++)
    {
        UILabel *myLabel = [textLabelsArray objectAtIndex:i];
        [myLabel.layer removeAllAnimations];
        [self performSelector:@selector(backAnimate:) withObject:myLabel afterDelay:i*0.7];
        [self performSelector:@selector(forwardAnimate:) withObject:myLabel afterDelay:(i+6)*0.7];
        
    }
    [self scheduleTimer];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
