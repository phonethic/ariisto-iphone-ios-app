//
//  AriistoAmenitiesViewController.m
//  Ariisto
//
//  Created by Rishi on 06/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoAmenitiesViewController.h"
#import "AriistoAmenitiesImageViewController.h"
#import "AriistoImageViewController.h"

NSUInteger kInternalNumberOfPages;
NSUInteger kExternalNumberOfPages;

@interface AriistoAmenitiesViewController ()
@property (assign) NSUInteger internalpage;
@property (assign) NSUInteger externalpage;
@end

@implementation AriistoAmenitiesViewController
@synthesize internalScrollView;
@synthesize internalPageControl;
@synthesize externalScrollView;
@synthesize externalPgeControl;
@synthesize externalBtn;
@synthesize amenitiesdata;
@synthesize viewExternalControllers;
@synthesize viewInternalControllers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSMutableDictionary *)fillData{
    
    NSMutableDictionary *tempData=[[NSMutableDictionary alloc]init];
    
    NSArray *InternalAmenitiesImages=[NSArray arrayWithObjects:
                                      [[NSArray alloc] initWithObjects:@"sevenone_internal_aminities.jpg",@"image",@"Living_Room_Description.png",nil], nil];
    
    NSArray *ExternalAmenitiesImages=[NSArray arrayWithObjects:
                                      [[NSArray alloc] initWithObjects:@"sevenone_external_aminities.jpg",@"text",@"ENTRANCE LOBBY",nil],nil];
    
    
    [tempData setObject:InternalAmenitiesImages forKey:@"internal"];
    [tempData setObject:ExternalAmenitiesImages forKey:@"external"];
    
    return tempData;
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    // if (self.AmenityType==2) {
    
    NSLog(@"controllers count %d",[viewExternalControllers count]);
    
    for (NSUInteger i =0; i < [viewExternalControllers count]; i++) {
        [self loadScrollViewWithPage:i type:2];
    }
    
    self.externalPgeControl.currentPage = 0;
    _externalpage = 0;
    [self.externalPgeControl setNumberOfPages:kExternalNumberOfPages];
    
    UIViewController *viewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
    if (viewController.view.superview != nil) {
        [viewController viewWillAppear:animated];
    }
    
    self.externalScrollView.contentSize = CGSizeMake(externalScrollView.frame.size.width * kExternalNumberOfPages, externalScrollView.frame.size.height);
    
    for (NSUInteger i =0; i < [viewInternalControllers count]; i++) {
        [self loadScrollViewWithPage:i type:1];
    }
    
    self.internalPageControl.currentPage = 0;
    _internalpage = 0;
    [self.internalPageControl setNumberOfPages:kInternalNumberOfPages];
    
    UIViewController *viewController2 = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
    if (viewController2.view.superview != nil) {
        [viewController2 viewWillAppear:animated];
    }
    
    self.internalScrollView.contentSize = CGSizeMake(internalScrollView.frame.size.width * kInternalNumberOfPages, internalScrollView.frame.size.height);
    
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    if (self.AmenityType==2) {
        UIViewController *eviewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
        if (eviewController.view.superview != nil) {
            [eviewController viewDidAppear:animated];
        }
    }
    else{
        UIViewController *viewController = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidAppear:animated];
        }
    }
	
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
	return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[internalBtn setImage:[UIImage imageNamed:@"internal_dark.png"] forState:UIControlStateNormal];
    //[externalBtn setImage:[UIImage imageNamed:@"external_light.png"] forState:UIControlStateNormal];
    //[internalBtn setUserInteractionEnabled:NO];
    //[externalBtn setUserInteractionEnabled:YES];
    
    amenitiesdata = [self fillData];
    
    
    kInternalNumberOfPages=[[self.amenitiesdata objectForKey:@"internal"] count];
    NSMutableArray *icontrollers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kInternalNumberOfPages; i++)
    {
        [icontrollers addObject:[NSNull null]];
    }
    
    self.viewInternalControllers = icontrollers;
    
    
    // a page is the width of the scroll view
    internalScrollView.clipsToBounds = YES;
    internalScrollView.scrollEnabled = YES;
    internalScrollView.pagingEnabled = YES;
    internalScrollView.contentSize = CGSizeMake(internalScrollView.frame.size.width * kInternalNumberOfPages, internalScrollView.frame.size.height + 280);
    internalScrollView.showsHorizontalScrollIndicator = NO;
    internalScrollView.showsVerticalScrollIndicator = NO;
    internalScrollView.scrollsToTop = YES;
    internalScrollView.delegate = self;
    internalScrollView.bounces = NO;
    internalScrollView.directionalLockEnabled = YES;
    internalPageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    
    
    kExternalNumberOfPages=[[self.amenitiesdata objectForKey:@"external"] count];
    NSLog(@"kexternalcount %d",kExternalNumberOfPages);
    NSMutableArray *econtrollers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kExternalNumberOfPages; i++)
    {
        [econtrollers addObject:[NSNull null]];
    }
    
    self.viewExternalControllers = econtrollers;
    
    
    // a page is the width of the scroll view
    externalScrollView.clipsToBounds = YES;
    externalScrollView.scrollEnabled = YES;
    externalScrollView.pagingEnabled = YES;
    externalScrollView.contentSize = CGSizeMake(externalScrollView.frame.size.width * kExternalNumberOfPages,externalScrollView.frame.size.height + 280);
    externalScrollView.showsHorizontalScrollIndicator = NO;
    externalScrollView.showsVerticalScrollIndicator = NO;
    externalScrollView.scrollsToTop = YES;
    externalScrollView.delegate = self;
    externalScrollView.bounces = NO;
    externalScrollView.directionalLockEnabled = YES;
    externalPgeControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    
    
    [self loadScrollViewWithPage:0 type:1];
    [self loadScrollViewWithPage:1 type:1];
    
    [self loadScrollViewWithPage:0 type:2];
    [self loadScrollViewWithPage:1 type:2];
    
    self.externalPgeControl.hidden=YES;
    self.externalScrollView.hidden=YES;

}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.AmenityType==2) {
        UIViewController *eviewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
        if (eviewController.view.superview != nil) {
            [eviewController viewWillDisappear:animated];
        }
    }
    else{
        UIViewController *viewController = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewWillDisappear:animated];
        }
        
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    if (self.AmenityType==2) {
        UIViewController *eviewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
        if (eviewController.view.superview != nil) {
            [eviewController viewDidDisappear:animated];
        }
    }else{
        UIViewController *viewController = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidDisappear:animated];
        }
    }
	
	[super viewDidDisappear:animated];
}

- (void)unloadScrollViewWithPage:(int)page type:(int)amtype {
    if (page < 0) return;
    if (amtype == 2) {
        if (page >= kExternalNumberOfPages) return;
    }else{
        if (page >= kInternalNumberOfPages) return;
    }
    
    if (self.AmenityType==2) {
        UIViewController *econtroller = [viewExternalControllers objectAtIndex:page];
        
        if ((NSNull *)econtroller != [NSNull null]) {
            if (nil != econtroller.view.superview) {
                [econtroller.view removeFromSuperview];
                econtroller.view=nil;
            }
            
            [viewExternalControllers replaceObjectAtIndex:page withObject:[NSNull null]];
        }
    }
    else{
        UIViewController *controller = [viewInternalControllers objectAtIndex:page];
        
        if ((NSNull *)controller != [NSNull null]) {
            if (nil != controller.view.superview) {
                [controller.view removeFromSuperview];
                controller.view=nil;
            }
            
            [viewInternalControllers replaceObjectAtIndex:page withObject:[NSNull null]];
        }
    }
    
}

- (void)viewDidUnload
{
    [self setInternalScrollView:nil];
    [self setInternalPageControl:nil];
    [self setExternalScrollView:nil];
    [self setExternalPgeControl:nil];
    [self setExternalBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)loadScrollViewWithPage:(int)page type:(int)amtype {
    NSLog(@"in page load %d",page);
    if (page < 0) return;
    
    if (amtype == 2) {
        if (page >= kExternalNumberOfPages) return;
    }else{
        if (page >= kInternalNumberOfPages) return;
    }
    
    
    if (amtype==2) {
        // replace the placeholder if necessary
        UIViewController *econtroller = [viewExternalControllers objectAtIndex:page];
        if ((NSNull *)econtroller == [NSNull null])
        {
            NSArray *numberItem=[[amenitiesdata valueForKey:@"external"] objectAtIndex:page];
            NSLog(@"imagename %@",[numberItem objectAtIndex:0]);
            econtroller = (UIViewController *)[[AriistoAmenitiesImageViewController alloc] initWithImageName:[numberItem objectAtIndex:0]];
            [viewExternalControllers replaceObjectAtIndex:page withObject:econtroller];
        }
        
        // add the controller's view to the scroll view
        if (econtroller.view.superview == nil) {
            CGRect frame = self.externalScrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            econtroller.view.frame = frame;
            [self.externalScrollView addSubview:econtroller.view];
        }
    }else{
        // replace the placeholder if necessary
        UIViewController *controller = [viewInternalControllers objectAtIndex:page];
        if ((NSNull *)controller == [NSNull null])
        {
            NSArray *numberItem=[[amenitiesdata valueForKey:@"internal"] objectAtIndex:page];
            NSLog(@"imagename %@",[numberItem objectAtIndex:0]);
            controller = (UIViewController *)[[AriistoAmenitiesImageViewController alloc] initWithImageName:[numberItem objectAtIndex:0]];
            [viewInternalControllers replaceObjectAtIndex:page withObject:controller];
        }
        
        // add the controller's view to the scroll view
        if (controller.view.superview == nil) {
            CGRect frame = self.internalScrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            controller.view.frame = frame;
            [self.internalScrollView addSubview:controller.view];
        }
    }
    
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (self.AmenityType==2) {
        UIViewController *eoldViewController = [viewExternalControllers objectAtIndex:_externalpage];
        UIViewController *enewViewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
        [eoldViewController viewDidDisappear:YES];
        [enewViewController viewDidAppear:YES];
        
        _externalpage = self.externalPgeControl.currentPage;
    }
    else{
        UIViewController *oldViewController = [viewInternalControllers objectAtIndex:_internalpage];
        UIViewController *newViewController = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
        [oldViewController viewDidDisappear:YES];
        [newViewController viewDidAppear:YES];
        
        _internalpage = self.internalPageControl.currentPage;
    }
	
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	if (self.AmenityType==2) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat epageWidth = externalScrollView.frame.size.width;
        int epage = floor((externalScrollView.contentOffset.x - epageWidth / 2) / epageWidth) + 1;
        if(externalPgeControl.currentPage != epage) {
            //[audioPlayer prepareToPlay];
            //[audioPlayer play];
        }
        if (self.externalPgeControl.currentPage != epage) {
            UIViewController *eoldViewController = [viewExternalControllers objectAtIndex:self.externalPgeControl.currentPage];
            UIViewController *enewViewController = [viewExternalControllers objectAtIndex:epage];
            [eoldViewController viewWillDisappear:YES];
            [enewViewController viewWillAppear:YES];
            self.externalPgeControl.currentPage = epage;
            [eoldViewController viewDidDisappear:YES];
            [enewViewController viewDidAppear:YES];
            _externalpage = epage;
        }
    }
    else{
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = internalScrollView.frame.size.width;
        int page = floor((internalScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if(internalPageControl.currentPage != page) {
            //[audioPlayer prepareToPlay];
            //[audioPlayer play];
        }
        if (self.internalPageControl.currentPage != page) {
            UIViewController *oldViewController = [viewInternalControllers objectAtIndex:self.internalPageControl.currentPage];
            UIViewController *newViewController = [viewInternalControllers objectAtIndex:page];
            [oldViewController viewWillDisappear:YES];
            [newViewController viewWillAppear:YES];
            self.internalPageControl.currentPage = page;
            [oldViewController viewDidDisappear:YES];
            [newViewController viewDidAppear:YES];
            _internalpage = page;
        }
    }
    
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    //ibtn.hidden=YES;
    //showTextPopUp.frame = CGRectMake(0, 1004, 768, 500);
    
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    //ibtn.hidden=NO;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)toggleBtnPressed:(id)sender {
    if([externalBtn.titleLabel.text isEqualToString:@"internal"])
    {
        [externalBtn setTitle: @"external" forState: UIControlStateNormal];
        [externalBtn setImage:[UIImage imageNamed:@"external.png"] forState:UIControlStateNormal];
        self.AmenityType=1;
        self.internalScrollView.hidden=NO;
        self.internalPageControl.hidden=NO;
        self.externalPgeControl.hidden=YES;
        self.externalScrollView.hidden=YES;
        self.internalPageControl.currentPage=0;
    } else {
        [externalBtn setTitle: @"internal" forState: UIControlStateNormal];
        [externalBtn setImage:[UIImage imageNamed:@"internal.png"] forState:UIControlStateNormal];
        self.AmenityType=2;
        self.internalScrollView.hidden=YES;
        self.internalPageControl.hidden=YES;
        self.externalPgeControl.hidden=NO;
        self.externalScrollView.hidden=NO;
        self.externalPgeControl.currentPage=0;
    }

    [self loadScrollViewWithPage:0 type:AmenityType];
    [self loadScrollViewWithPage:1 type:AmenityType];

}

@end
