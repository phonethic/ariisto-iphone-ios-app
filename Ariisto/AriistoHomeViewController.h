//
//  AriistoHomeViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AriistoHomeViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    AVAudioPlayer *audioPlayer;
    NSArray  *homeImagesArray;
    NSMutableArray *viewControllers;
    NSUInteger kNumberOfPages;
    BOOL pageControlUsed;
    NSMutableArray *textLabelsArray;
    NSTimer *timer;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *speakerBtn;
@property (strong, nonatomic) IBOutlet UISlider *volSlider;
@property (strong, nonatomic) IBOutlet UIButton *settingBtn;

@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) NSMutableArray *textLabelsArray;

- (IBAction)changePage:(id)sender;
- (IBAction)speakerBtnPressed:(id)sender;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
- (IBAction)volChanged:(id)sender;
- (IBAction)settingBtnPressed:(id)sender;
@end
