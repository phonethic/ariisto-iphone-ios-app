//
//  AriistoAppointmentViewController.h
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoAppointmentViewController : UIViewController <UIScrollViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UITextField *nameTxtField;
@property (strong, nonatomic) IBOutlet UITextField *emailTxtField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxtField;
@property (strong, nonatomic) IBOutlet UIScrollView *appointmentScrollView;


- (IBAction)backBtnPressed:(id)sender;
- (IBAction)sebdBtnPressed:(id)sender;


@end
