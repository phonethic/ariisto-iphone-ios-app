//
//  AriistoGalleryViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 27/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
@class AriistoProjectsData;
@interface AriistoGalleryViewController : UIViewController<UIScrollViewDelegate,AVAudioPlayerDelegate,NSXMLParserDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    AVAudioPlayer *audioPlayer;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    int status;
    NSMutableArray *projects;
    BOOL linkFound;
    NSString *ituneLink;
    NSUInteger kNumberOfPages;
    NSArray  *projectsImagesArray;
    AriistoProjectsData *projectsObj;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
	NSError  *connectionError;
}


@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, readwrite) NSUInteger kNumberOfPages;
@property (nonatomic, copy) NSString *ituneLink;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *detailsBtn;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *statusIndicator;

- (void)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page;
- (void)unloadScrollViewWithPage:(int)page;
- (IBAction)detailsBtnPressed:(id)sender;

@end
