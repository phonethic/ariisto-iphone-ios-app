//
//  AriistoDetailViewController.m
//  Ariisto
//
//  Created by Rishi on 29/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoDetailViewController.h"
#import "AriistoLocationViewController.h"
#import "AriistoFloorplanViewController.h"
#import "AriistoLayoutplanViewController.h"
#import "AriistoPDFViewController.h"
#import "AriistoAppointmentViewController.h"
#import "AriistoElevationViewController.h"
#import "AriistoMapViewController.h"
#import "AriistoGridGalleryViewController.h"
#import "AriistoAmenitiesViewController.h"
#import "AriistoMainContactViewController.h"

@interface AriistoDetailViewController ()
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;
@end

@implementation AriistoDetailViewController
@synthesize backBtn;
@synthesize headerView;
@synthesize scrollView;
@synthesize pagecontrol;
@synthesize arrowBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//#ifdef _GRID_VIEW_
//	scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
//    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
//#else
//    scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height*2), scrollView.frame.size.width,scrollView.frame.size.height-headerView.frame.size.height);
//    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
//#endif
//    //[scrollView bringSubviewToFront:pagecontrol];
//}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSArray *buttonImages=[[NSArray alloc] initWithObjects:@"Location",@"Googlemap",@"Elevation",@"Layoutplan",@"Floorplan",@"Amenities",@"Gallery",@"PDF",@"Appointments",@"Contactus", nil ];
    
	[scrollView setBackgroundColor:[UIColor clearColor]];
	[scrollView setCanCancelContentTouches:NO];
	scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	scrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
	scrollView.scrollEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.directionalLockEnabled = YES;
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    
    pagecontrol.hidden = TRUE;
    
#ifdef _GRID_VIEW_
	scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#else
    scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height*2), scrollView.frame.size.width,scrollView.frame.size.height-headerView.frame.size.height);
    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#endif
    
	
	
	// load all the images from our bundle and add them to the scroll view
    int numberOfbuttons=[buttonImages count];
    double numberOfbuttonsperpage=6;
    int indexofButtonImage=0;
    
    int pageCount = (int)ceil(numberOfbuttons/numberOfbuttonsperpage);
    NSLog(@"page count = %d ", pageCount);
    
    pagecontrol.numberOfPages = pageCount;
    pagecontrol.currentPage = 0;
    
	for (int i = 1; i <= pageCount; i++)
	{
		int tag = i * 100;
        
        UIView *view=[[UIView alloc] init];
		CGRect rect;// = view.frame;
        view.backgroundColor=[UIColor clearColor];
        
		rect.size.height = scrollView.frame.size.height;
		rect.size.width = scrollView.frame.size.width;
		view.frame = rect;
		view.tag = i;	// tag our images for later use when we place them in serial fashion
		
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_background.png"]];
        img.frame = view.frame;
        img.alpha = 0.9;
        [view addSubview:img];
        //NSLog(@"x=%f y=%f w=%f h=%f ",scrollView.frame.origin.x,scrollView.frame.origin.y,scrollView.frame.size.width,scrollView.frame.size.height );
        
#ifdef _GRID_VIEW_
        int X=45,Y=35;
        int Hspace=45;
        int Vspace=5;
#else
        int X=25,Y=30;
        int Hspace=25;
        int Vspace=25;
#endif
        
        int width=72;
        int height=72;
        int labelHeight=20;
        int labelWidth=110;
        UIButton *button;
        UILabel *label;
        
        CGRect frame;
        for (int b=0;b<numberOfbuttonsperpage; b++) {
            
            
            frame = CGRectMake(X, Y, width, height);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = frame;
            button.backgroundColor=[UIColor clearColor];
            [button setTitle:[buttonImages objectAtIndex:indexofButtonImage+b] forState:UIControlStateNormal];
            // [button setImage:[UIImage imageNamed:[buttonImages objectAtIndex:indexofButtonImage+b]] forState:UIControlStateNormal];
            UIImage *image =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[buttonImages objectAtIndex:indexofButtonImage+b] ofType:@"png" ]];
            [button setImage:image forState:UIControlStateNormal];
            button.tag = tag+b;
            [button addTarget:self action:@selector(buttonClicked:)forControlEvents:UIControlEventTouchUpInside];
            
            
            
            frame = CGRectMake(X-25, Y+height, labelWidth, labelHeight);
            label = [[UILabel alloc]initWithFrame:frame];
            label.backgroundColor=[UIColor clearColor];
            label.text=[buttonImages objectAtIndex:indexofButtonImage+b];
            label.textAlignment=UITextAlignmentCenter;
            label.textColor = [UIColor whiteColor];
            label.hidden = TRUE;
            [view addSubview:button];
            [view addSubview:label];
            
            button=nil;
            label=nil;
            int NextX = X+width+Hspace;
            //NSLog(@"NextX %d %f",NextX,view.frame.size.width-100);
            
            if (NextX < view.frame.size.width-60)
            {
                X=NextX;
            }
            else
            {
            #ifdef _GRID_VIEW_
                X=45;
            #else
                X=25;
            #endif
                Y=Y+height+labelHeight+Vspace;
            }
            
        }
        
        [scrollView addSubview:view];
        indexofButtonImage=numberOfbuttonsperpage;
        numberOfbuttonsperpage=numberOfbuttons-numberOfbuttonsperpage;
	}
    
    [self layoutScrollImages:pageCount];
    
    [self performSelector:@selector(arrowBtnPressed:) withObject:nil afterDelay:0.6];


}

- (void)layoutScrollImages:(int)count
{
	UIView *view = nil;
	NSArray *subviews = [scrollView subviews];
    
	// reposition all subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (scrollView.frame.size.width);
		}
	}
	
	// set the content size so it can be scrollable
	[scrollView setContentSize:CGSizeMake((count * scrollView.frame.size.width), scrollView.frame.size.height)];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pagecontrol.currentPage != page) {
        pagecontrol.currentPage = page;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}


- (void)viewDidUnload
{
    [self setBackBtn:nil];
    [self setScrollView:nil];
    [self setHeaderView:nil];
    [self setPagecontrol:nil];
    [self setArrowBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)arrowBtnPressed:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSLog(@"You clicked %d",btn.tag);
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"folding-door-open" ofType:@"mp3"]] error:&error];
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
        [audioPlayer play];
    }

    if (!BUTTONFLAG) {
        [arrowBtn setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:@"Drop_Down" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, headerView.frame.size.height, scrollView.frame.size.width,scrollView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, scrollView.frame.size.height+headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
        [UIView commitAnimations];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:arrowBtn cache:YES];
        [UIView commitAnimations];
        BUTTONFLAG=TRUE;
        
    }else{
        pagecontrol.hidden = TRUE;
        [UIView beginAnimations:@"Drop_Up" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
        [UIView commitAnimations];
        BUTTONFLAG=FALSE;
        
    }

}

- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID isEqualToString:@"Drop_Up"])
    {
        [arrowBtn setImage:[UIImage imageNamed:@"up.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:arrowBtn cache:YES];
        [UIView commitAnimations];
    } else {
        pagecontrol.hidden = FALSE;
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

-(void)buttonClicked:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    NSLog(@"You clicked %@",btn.titleLabel.text);

    if([btn.titleLabel.text isEqualToString:@"Location"]) {
        AriistoLocationViewController *locationviewController = [[AriistoLocationViewController alloc] initWithNibName:@"AriistoLocationViewController" bundle:nil] ;
        locationviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:locationviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Googlemap"]) {
        AriistoMapViewController *mapviewController = [[AriistoMapViewController alloc] initWithNibName:@"AriistoMapViewController" bundle:nil] ;
        mapviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:mapviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Elevation"]) {
        AriistoElevationViewController *elevationviewController = [[AriistoElevationViewController alloc] initWithNibName:@"AriistoElevationViewController" bundle:nil] ;
        elevationviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:elevationviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Layoutplan"]) {
        AriistoLayoutplanViewController *layoutplanviewController = [[AriistoLayoutplanViewController alloc] initWithNibName:@"AriistoLayoutplanViewController" bundle:nil] ;
        layoutplanviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:layoutplanviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Floorplan"]) {
        AriistoFloorplanViewController *floorplanviewController = [[AriistoFloorplanViewController alloc] initWithNibName:@"AriistoFloorplanViewController" bundle:nil] ;
        floorplanviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:floorplanviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Amenities"]) {
        AriistoAmenitiesViewController *aminitiesviewController = [[AriistoAmenitiesViewController alloc] initWithNibName:@"AriistoAmenitiesViewController" bundle:nil] ;
        aminitiesviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:aminitiesviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Gallery"]) {
        AriistoGridGalleryViewController *pdfviewController = [[AriistoGridGalleryViewController alloc] initWithNibName:@"AriistoGridGalleryViewController" bundle:nil] ;
        pdfviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:pdfviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"PDF"]) {
        AriistoPDFViewController *pdfviewController = [[AriistoPDFViewController alloc] initWithNibName:@"AriistoPDFViewController" bundle:nil] ;
        pdfviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:pdfviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Appointments"]) {
        AriistoAppointmentViewController *appointmentviewController = [[AriistoAppointmentViewController alloc] initWithNibName:@"AriistoAppointmentViewController" bundle:nil] ;
        appointmentviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:appointmentviewController animated:YES];
    } else if([btn.titleLabel.text isEqualToString:@"Contactus"]) {
        AriistoMainContactViewController *contactusviewController = [[AriistoMainContactViewController alloc] initWithNibName:@"AriistoMainContactViewController" bundle:nil] ;
        contactusviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:contactusviewController animated:YES];
    }

}


- (IBAction)changePage:(id)sender {
    int page = pagecontrol.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;

}


@end
