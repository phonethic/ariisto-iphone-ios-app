//
//  AriistoLayoutplanViewController.m
//  Ariisto
//
//  Created by Rishi on 31/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoLayoutplanViewController.h"

@interface AriistoLayoutplanViewController ()

@end

@implementation AriistoLayoutplanViewController
@synthesize layoutScrollView;
@synthesize backBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"layoutplan_img.png"]];
    imgView.frame = CGRectMake(0, 0, 320, 380);
    
	layoutScrollView.maximumZoomScale = 3.0;
    layoutScrollView.minimumZoomScale = 1.0;
	layoutScrollView.clipsToBounds = YES;
	layoutScrollView.delegate = self;
	layoutScrollView.scrollEnabled = YES;
    [layoutScrollView setZoomScale:1.0 animated:YES];
    [layoutScrollView addSubview:imgView];

}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imgView;
}


- (void)viewDidUnload
{
    [self setLayoutScrollView:nil];
    [self setBackBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}
@end
