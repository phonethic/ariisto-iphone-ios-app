//
//  AriistoAmenitiesImageViewController.m
//  Ariisto
//
//  Created by Rishi on 07/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoAmenitiesImageViewController.h"

@interface AriistoAmenitiesImageViewController ()

@end

@implementation AriistoAmenitiesImageViewController
@synthesize imageView;

- (id)initWithImageName:(NSString *) name {
    if (self = [super initWithNibName:@"AriistoAmenitiesImageViewController" bundle:nil])
    {
        NSLog(@"got image as %@",name);
        imageNameString = name;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.imageView setImage:[UIImage imageNamed:imageNameString]];
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
