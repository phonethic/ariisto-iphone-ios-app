//
//  AriistoSiteViewController.h
//  Ariisto
//
//  Created by Sagar Mody on 02/09/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AriistoSiteViewController : UIViewController <UIWebViewDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UIWebView *ariistoWebView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

- (IBAction)backBtnPressed:(id)sender;
@end
