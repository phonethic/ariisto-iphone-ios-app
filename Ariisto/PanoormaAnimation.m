//
//  PanoormaAnimation.m
//  BrochureApplication
//
//  Created by Kshitij Ghule on 06/09/11.
//

#import "PanoormaAnimation.h"
#import <QuartzCore/QuartzCore.h>
#import "AriistoAppDelegate.h"

@implementation PanoormaAnimation
@synthesize webView;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"vr5_script"]isDirectory:NO]]];
	[webView setUserInteractionEnabled:YES];
	[self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (IBAction)enterApp:(id)sender {
    [myAppDelegate panaromaStop];
    //[self.view removeFromSuperview];
	
}
@end
