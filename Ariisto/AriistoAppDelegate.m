//
//  AriistoAppDelegate.m
//  Ariisto
//
//  Created by Sagar Mody on 26/08/12.
//  Copyright (c) 2012 sagarkm@rediffmail.com. All rights reserved.
//

#import "AriistoAppDelegate.h"

#import "AriistoViewController.h"
#import "AriistoHomeViewController.h"
#import "AriistoGalleryViewController.h"
#import "AriistoMyProjectViewController.h"
#import "AriistoContactViewController.h"
#import "AriistoSplashVideoViewController.h"
#import "PanoormaAnimation.h"

@implementation AriistoAppDelegate
@synthesize networkavailable;
@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize animationviewController=_animationviewController;
@synthesize panoController;

-(void)animationStop
{
    [self.animationviewController.view removeFromSuperview];
    
    panoController = [[PanoormaAnimation alloc]initWithNibName:@"PanoormaAnimation" bundle:nil];
	[self.window addSubview:panoController.view];
    
}

-(void)panaromaStop
{
    [self.panoController.view removeFromSuperview];
    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.window.rootViewController = self.tabBarController;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
    //self.tabBarController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.tabBarController.selectedIndex = 2;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

-(void) addCustomTabBarIcons
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 4.9) {
        //iOS 5
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i_gray.png",iLoop + 1]]];        
            [imgView setFrame:CGRectMake(iLoop * 80, 0, 80, 49)];
            imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
            UIViewAutoresizingFlexibleWidth        |
            UIViewAutoresizingFlexibleRightMargin  |
            UIViewAutoresizingFlexibleTopMargin    |
            UIViewAutoresizingFlexibleHeight       |
            UIViewAutoresizingFlexibleBottomMargin ;
            [self.tabBarController.tabBar insertSubview:imgView atIndex:1]; 
            imgView.tag = 1000;
        }
    }
    else {
        //iOS 4.whatever and below
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i_gray.png",iLoop + 1]]];        
            [imgView setFrame:CGRectMake(iLoop * 80, 0, 80, 49)];
            imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
            UIViewAutoresizingFlexibleWidth        |
            UIViewAutoresizingFlexibleRightMargin  |
            UIViewAutoresizingFlexibleTopMargin    |
            UIViewAutoresizingFlexibleHeight       |
            UIViewAutoresizingFlexibleBottomMargin ;
            [self.tabBarController.tabBar insertSubview:imgView atIndex:0]; 
            imgView.tag = 1000;
			
        }
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i.png",3]]];
	
	[imgView setFrame:CGRectMake(80*2, 0, 80, 49)];
	[self.tabBarController.tabBar addSubview:imgView]; 
    
}

// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    // [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
    for (UIView *view in tabBarController.tabBar.subviews){
		
		if([view isKindOfClass:[UIImageView class]])
		{
			if(view.tag != 1000)
				[view removeFromSuperview];
		}
	}
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i.png",self.tabBarController.selectedIndex+1]]];
	[self.tabBarController.tabBar setBackgroundColor:[UIColor blackColor]];
    [imgView setFrame:CGRectMake(self.tabBarController.selectedIndex * 80, 0, 80, 49)];
    imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
    UIViewAutoresizingFlexibleWidth        |
    UIViewAutoresizingFlexibleRightMargin  |
    UIViewAutoresizingFlexibleTopMargin    |
    UIViewAutoresizingFlexibleHeight       |
    UIViewAutoresizingFlexibleBottomMargin ;
    [self.tabBarController.tabBar addSubview:imgView]; 
    
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];    
    
    UIViewController *viewController1 = [[AriistoHomeViewController alloc] initWithNibName:@"AriistoHomeViewController" bundle:nil] ;
    
    UIViewController *viewController2 = [[AriistoGalleryViewController alloc] initWithNibName:@"AriistoGalleryViewController" bundle:nil] ;
    
    UIViewController *viewController3 = [[AriistoMyProjectViewController alloc] initWithNibName:@"AriistoMyProjectViewController" bundle:nil] ;
    
    UIViewController *viewController4 = [[AriistoContactViewController alloc] initWithNibName:@"AriistoContactViewController" bundle:nil];
    
        
    UINavigationController* homeNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController1];
    homeNavigationController.navigationBar.tintColor = [UIColor blackColor];
    homeNavigationController.navigationBar.tag = 1;
    
    UINavigationController* galleryNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController2];
    galleryNavigationController.navigationBar.tintColor = [UIColor blackColor];
    galleryNavigationController.navigationBar.tag = 2;
    
    UINavigationController* myprojectNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController3];
    myprojectNavigationController.navigationBar.tintColor = [UIColor blackColor];
    myprojectNavigationController.navigationBar.tag = 3;
    
    UINavigationController* contactNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController4 ];
    contactNavigationController.navigationBar.tintColor = [UIColor blackColor];
    contactNavigationController.navigationBar.tag = 4;
    
    
    NSMutableArray *viewControllers;
    viewControllers = [[NSMutableArray alloc] init];
    
    //Tab 1
    [viewControllers addObject:homeNavigationController];
    //Tab 2
    [viewControllers addObject:myprojectNavigationController];
    //Tab 3
    [viewControllers addObject:galleryNavigationController];
    //Tab 4
    [viewControllers addObject:contactNavigationController];

    
    self.tabBarController = [[UITabBarController alloc] init];
    
    self.tabBarController.viewControllers = viewControllers;
	
    
    self.tabBarController.delegate = self; 
    
    [self addCustomTabBarIcons];
        
    AriistoSplashVideoViewController *splashviewController = [[AriistoSplashVideoViewController alloc] initWithNibName:@"AriistoSplashVideoViewController" bundle:nil];
    self.animationviewController = splashviewController;
    
#ifdef NO_STARTING_ANIMATION
    self.window.rootViewController = self.tabBarController;
    self.tabBarController.selectedIndex = 2;
#else
    [self.window addSubview:self.animationviewController.view];
#endif

    [self.window makeKeyAndVisible];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
